package aba.hhschool.oldone;

public class Task4 {

    /*
    Задача 4
    В некоторых числах можно найти последовательности цифр,
    которые в сумме дают 10. К примеру, в числе 3523014 целых
    четыре таких последовательности:
            3523014
            3523014
            3523014
            3523014

    Можно найти и такие замечательные числа, каждая цифра которых
    входит в по крайней мере одну такую последовательность.
    Например, 3523014 является замечательным числом, а 28546 — нет
    (в нём нет последовательности цифр, дающей в сумме 10 и при этом включающей 5).

    Найдите количество этих замечательных чисел в интервале [1, 6500000] (обе границы — включительно).
    */

    static private StringBuilder builder = new StringBuilder();
    static private boolean[] flagArr = new boolean[8];
    static private int sum;
    static private int result;

    static private boolean isBeauty(Integer val)
    {
        //Обнуление переменных
        builder.setLength(0);
        builder.append(val);
        for (int i = 0; i< flagArr.length; i++)
        {
            flagArr[i] = false;
        }


        //Для всех символов
        for (int i=0;i<builder.length();i++)
        {
            sum = 0;
            for(int j = i; j<builder.length()&&sum<=10; j ++)
            {
                sum += Integer.parseInt(""+builder.charAt(j));
                if (sum==10)
                {
                    for(int k=i;k<=j;k++)
                    {
                        flagArr[k] = true;
                    }
                }
            }
        }

        for (int i = 0; i< builder.length(); i++)
        {
            if(flagArr[i] == false)
                return false;
        }
        return true;
    }

    public static void execute()
    {
        for (int i=1;i<=6500000;i++)
            if (isBeauty(i)) result++;
        System.out.println("Задание 4:");
        System.out.println("    количество замечательных чисел, найденных в интервале [1, 6500000]: " + result);

    }
}
