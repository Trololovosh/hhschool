package aba.hhschool.oldone;

import java.math.BigInteger;

import aba.hhschool.Main;

public class Task3 {

    /*
    Задача 3
    Если мы возьмем 47, перевернем его и сложим, получится 47 + 74 = 121 — число-палиндром.

    Если взять 349 и проделать над ним эту операцию три раза, то тоже получится палиндром:
    349 + 943 = 1292
    1292 + 2921 = 4213
    4213 + 3124 = 7337

    Найдите количество положительных натуральных чисел меньших 13036 таких,
    что из них нельзя получить палиндром за 50 или менее применений описанной операции
    (операция должна быть применена хотя бы один раз).
    */

    private static int result = 0;
    private static int errors = 0;

    private static boolean isPolyndrom(BigInteger val)
    {
        StringBuilder builder = new StringBuilder(val.toString());
        if (builder.toString().equals(builder.reverse().toString()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static void doSum(int currentIteration)
    {
        BigInteger val = BigInteger.valueOf(currentIteration);
        for (int j = 1; j <= 50; j++)
        {
            try {
                val = val.add(new BigInteger(new StringBuilder(val.toString()).reverse().toString()));
            }
            catch (Exception e)
            {
                System.out.println("Текущая итерация: " + currentIteration);
                System.out.println("Текущий шаг: " + j);
                throw e;
            }
            if (isPolyndrom(val))
            {
                result++;
                return;
            }
        }
    }

    public static void execute()
    {

        for (int i = 1; i<=13036; i++)
        {
            doSum(i);
        }
        System.out.println("Задание 3:");
        System.out.println("    Число полученных полиндромов = " + result);
        if (Main.IS_DEBUG == true)
            System.out.println("    Число полученных ошибок = " + errors);
    }
}
