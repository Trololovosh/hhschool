package aba.hhschool.oldone;

import java.util.ArrayList;
import java.util.Arrays;

public class Task1 {

/*
    Задача 1
    Определим функцию P(n,k) следующим образом: P(n,k) = 1,
    если n может быть представлено в виде суммы k простых чисел
    (простые числа в записи могут повторяться, 1 не является простым числом)
    и P(n,k) = 0 в обратном случае.
    К примеру, P(10,2) = 1, т.к. 10 может быть представлено в виде суммы 3 + 7 или 5 + 5, а P(11,2) = 0,
    так как никакие два простых числа не могут дать в сумме 11.
    Определим функцию S(n) как сумму значений функции P(i,k) для всех i и k,
    таких что 1≤i≤n, 1≤k≤n. Таким образом,
    S(2) = P(1,1) + P(2,1) + P(1,2) + P(2,2) = 1, S(10) = 20, S(1000) = 248838.

    Найдите S(11519).
*/

    private static final int finalS = 11519;
    private static int[] simpleNumbers;

    static {//find simple numbers
        boolean[] primes = new boolean[finalS + 1];
        Arrays.fill(primes, true);
        primes[0] = primes[1] = false;
        for (int i = 2; i < primes.length; ++i) {
            if (primes[i]) {
                for (int j = 2; i * j < primes.length; ++j) {
                    primes[i * j] = false;
                }
            }
        }
        int count = 0;
        for (int i = 2; i < primes.length;i++)
        {
            if (primes[i])
            {
                count++;
            }
        }
        simpleNumbers = new int[count];
        int j = 0;
        for (int i = 2; i < primes.length;i++) {
            if (primes[i])
            {
                simpleNumbers[j]=i;
                j++;
            }
        }
    }

    private static void printIntArray(int[] val)
    {
        System.out.print(simpleNumbers[0]);
        for(int i=1;i<simpleNumbers.length;i++)
        {
            System.out.print(", " + simpleNumbers[i]);
        }
        System.out.println();
    }

    public static void execute() {
        printIntArray(simpleNumbers);
        System.out.println(simpleNumbers.length);
    }
}
