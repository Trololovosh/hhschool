package aba.hhschool.oldone;

import aba.hhschool.*;

public class Task5 {

    /*
    Задача 5
    Дано равенство, в котором цифры заменены на буквы:
    ruzw + rrsw = swss

    Найдите сколько у него решений,
    если различным буквам соответствуют
    различные цифры (ведущих нулей в числе не бывает).
    */
    public static void execute()
    {
        //ruzw + rrsw = swss
        char r;
        char u;
        char z;
        char w;
        char s;
        int sum = 0;
        int first;
        int last;
        int result;
        for (r='1';r<='9';r++)
        {
            for (u='0';u<='9';u++)
            {
                for (z='0';z<='9';z++)
                {
                    for (w='0';w<='9';w++)
                    {
                        for (s='1';s<='9';s++) {
                            first = Integer.parseInt("" + r + u + z + w);
                            last = Integer.parseInt("" + r + r + s + w);
                            result = Integer.parseInt("" + s + w + s + s);
                            if (first+last==result) {
                                sum++;
                                if (Main.IS_DEBUG == true)
                                    System.out.println("" + r + u + z + w + " + " + r + r + s + w + " = " + s + w + s + s);
                            }
                        }
                    }
                }
            }
        }
        System.out.println("Задание 5:");
        System.out.println("    Числел, соответствующих ruzw + rrsw = swss: " + sum);

    }
}
