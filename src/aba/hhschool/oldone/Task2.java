package aba.hhschool.oldone;

import java.math.BigInteger;
import java.util.HashSet;

public class Task2 {

        /*
            Задача 2
        Рассмотрим все возможные числа ab для 1<a<6 и 1<b<6:
        22=4, 23=8, 24=16, 25=32 32=9, 33=27, 34=81, 35=243 42=16,
        43=64, 44=256, 45=1024, 52=25, 53=125, 54=625, 55=3125
        Если убрать повторения, то получим 15 различных чисел.

        Сколько различных чисел ab для 2<a<149 и 2<b<126?
        Ответ
        */

    private static HashSet<BigInteger> set = new HashSet<>();
    private static final int finalA = 149;
    private static final int finalB = 126;

    public static void execute()
    {
        for (int a = 2; a<finalA; a++)
        {
            for (int b = 2; b<finalB; b++)
            {
                set.add(BigInteger.valueOf(a).pow(b));
            }
        }
        System.out.println("Задание 2:");
        System.out.println("    Число различных чисел для 2<a<"+finalA+" и 2<b<"+finalB+" = "+set.size());
    }

}
