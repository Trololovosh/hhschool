package aba.hhschool;

import java.math.BigInteger;

public class Temp {
    private static BigInteger factorial (int x)
    {
        if (x<0) throw new IllegalArgumentException("x должен быть >=0");
        BigInteger fact=BigInteger.valueOf(1);
        for (int i=2; i<=x;i++)
            fact = fact.multiply(BigInteger.valueOf(i));
        return fact;
    }

    private static boolean combine (int n, int k)
    {
        BigInteger factn = factorial(n);
        BigInteger factk = factorial(k);
        BigInteger factnk = factorial(n-k);
        BigInteger threshold = BigInteger.valueOf(1000000);
        if (factn.divide(factk.multiply(factnk)).compareTo(threshold)>0) return true;
        else return false;
    }

    public static void execute()
    {
        int result = 0;
        for (int n = 1;n<132;n++)
        {
            for(int k = 1;k<n;k++)
            {
                if (combine(n,k)) result++;
            }
        }
        System.out.println("задача 5: число сочетаний: "+result);
    }
}
